import mincemeat
import glob
from time import time

beforeTime = time()

text_files = glob.glob('hw3data/*')

def file_contents(file_name):
    f = open(file_name, encoding='utf8')
    try:
        return f.read()
    finally:
        f.close()

source = dict((file_name, file_contents(file_name)) for file_name in text_files)
#source = { text_files[0]: file_contents(text_files[0])}

# setup map and reduce functions
def mapfn(key, file_contents):
    import modifyterm
    for line in file_contents.splitlines():
        (paperid, authors, title) = line.split(":::")
        for author in authors.split("::"):
            for term in title.split():
                modifiedTerm = modifyterm.modifyTerm(term)
                if len(modifiedTerm) > 0:
                    yield author, modifiedTerm

def reducefn(author, terms):
    import collections
    tmp = collections.Counter(terms)
    return sorted(tmp.items(), key=lambda item: item[1], reverse=True)

# start the server
s = mincemeat.Server()

# The data source can be any dictionary-like object
s.datasource = source
s.mapfn = mapfn
s.reducefn = reducefn

# Write data to file
results = s.run_server(password="changeme")

with open("result.txt", "w", encoding="utf-8") as output:
    for author in sorted(results.keys()):
        print(author, end='', file=output)
        for item in results[author]:
            print("::: {}: {}".format(item[0], item[1]), end='', file=output)
        print(file=output)

print("It took {} seconds to compute!!".format(time() - beforeTime))
