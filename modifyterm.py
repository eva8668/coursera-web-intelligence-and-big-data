import stopwords
import re

def modifyTerm(term):
    result = re.sub(r'\W+', '', term.lower())
    if result in stopwords.allStopWords:
        return ""
    elif len(result) == 1:
        return ""
    else:
        return result
